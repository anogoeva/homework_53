import React, {useState} from "react";
import {nanoid} from "nanoid";
import Task from "./Components/Task/Task";

const App = () => {
    const [tasks, setTasks] = useState([
        {text: 'Buy milk', id: 1},
        {text: 'Walk with dog', id: 2},
        {text: 'Do homework', id: 3}
    ]);

    const [task, setTask] = useState({
        text: ''
    });

    const onInputTextareaChange = e => {
        const {name, value} = e.target;
        setTask(prev => ({
            ...prev,
            [name]: value
        }));
    };

    const addTask = () => {
        setTasks([...tasks, {text: task.text, id: nanoid()}]);
    };

    const deleteTask = id => {
        setTasks(tasks.filter(t => t.id !== id));
    };

    let taskComponents;
    taskComponents = tasks.map(task => (
        <Task
            key={task.id}
            text={task.text}
            id={task.id}
            onDelete={() => deleteTask(task.id)}
        />
    ));


    let taskText = task ? task.text : '';

    return (
        <div>
            <div>
                <form className='form'>
                    <h3>To-do list</h3>
                    <div>
                        <input type="text" className="inp" onChange={onInputTextareaChange} value={taskText} name="text"/>
                        <button className="btn" type="button" onClick={addTask}>Add task</button>
                    </div>
                </form>
            </div>
            {taskComponents}
        </div>
    )
};

export default App;
